﻿namespace DnD4F_Character_Creator.Classes
{
    class Character
    {
        public Race Race
        {
            get; set;
        }

        public Background Background
        {
            get; set;
        }

        public Class Class
        {
            get; set;
        }

        public string Alignment
        {
            get; set;
        }

        public string CharacterName
        {
            get; set;
        }
        public string PlayerName
        {
            get; set;
        }

        public string[] Languages = { "Common" };

        public int Age
        {
            get; set;
        }
        public string Sex
        {
            get; set;
        }
        public int Height
        {
            get; set;
        }
        public int Weight
        {
            get; set;
        }
        public string HairColor
        {
            get; set;
        }
        public string EyeColor
        {
            get; set;
        }
        public string SkinColor
        {
            get; set;
        }

        public int Strength
        {
            get; set;
        }
        public int Dexterity
        {
            get; set;
        }
        public int Constitution
        {
            get; set;
        }
        public int Intelligence
        {
            get; set;
        }
        public int Wisdom
        {
            get; set;
        }
        public int Charisma
        {
            get; set;
        }

        public int GetCombinedStrength()
        {
            return Strength + Race.StrengthBonus;
        }
        public int GetCombinedDexterity()
        {
            return Dexterity + Race.DexterityBonus;
        }
        public int GetCombinedConstitution()
        {
            return Constitution + Race.ConstitutionBonus;
        }
        public int GetCombinedIntelligence()
        {
            return Intelligence + Race.IntelligenceBonus;
        }
        public int GetCombinedWisdom()
        {
            return Wisdom + Race.WisdomBonus;
        }
        public int GetCombinedCharisma()
        {
            return Charisma + Race.CharismaBonus;
        }

        public readonly int ProficiencyBonus = 2;
        public readonly int PassivePerception = 5;
        private readonly int Speed = 30;

        public int GetCombinedProficiencyBonus()
        {
            return ProficiencyBonus;
        }
        public int GetCombinedPassivePerception()
        {
            int returnValue = 0;

            if (PerceptionProficient)
            {
                returnValue += 2;
            }

            return returnValue + PassivePerception + (GetCombinedWisdom() / 2);
        }
        public int GetCombinedSpeed()
        {
            return Speed + Race.SpeedBonus;
        }

        public int Acrobatics
        {
            get; set;
        }
        public int AnimalHandling
        {
            get; set;
        }
        public int Arcana
        {
            get; set;
        }
        public int Athletics
        {
            get; set;
        }
        public int Deception
        {
            get; set;
        }
        public int History
        {
            get; set;
        }
        public int Insight
        {
            get; set;
        }
        public int Intimidation
        {
            get; set;
        }
        public int Investigation
        {
            get; set;
        }
        public int Medicine
        {
            get; set;
        }
        public int Nature
        {
            get; set;
        }
        public int Perception
        {
            get; set;
        }
        public int Performance
        {
            get; set;
        }
        public int Persuasion
        {
            get; set;
        }
        public int Religion
        {
            get; set;
        }
        public int SleightOfHand
        {
            get; set;
        }
        public int Stealth
        {
            get; set;
        }
        public int Survival
        {
            get; set;
        }

        public bool AcrobaticsProficient
        {
            get; set;
        }
        public bool AnimalHandlingProficient
        {
            get; set;
        }
        public bool ArcanaProficient
        {
            get; set;
        }
        public bool AthleticsProficient
        {
            get; set;
        }
        public bool DeceptionProficient
        {
            get; set;
        }
        public bool HistoryProficient
        {
            get; set;
        }
        public bool InsightProficient
        {
            get; set;
        }
        public bool IntimidationProficient
        {
            get; set;
        }
        public bool InvestigationProficient
        {
            get; set;
        }
        public bool MedicineProficient
        {
            get; set;
        }
        public bool NatureProficient
        {
            get; set;
        }
        public bool PerceptionProficient
        {
            get; set;
        }
        public bool PerformanceProficient
        {
            get; set;
        }
        public bool PersuasionProficient
        {
            get; set;
        }
        public bool ReligionProficient
        {
            get; set;
        }
        public bool SleightOfHandProficient
        {
            get; set;
        }
        public bool StealthProficient
        {
            get; set;
        }
        public bool SurvivalProficient
        {
            get; set;
        }

        public int GetCombinedAcrobatics()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (AcrobaticsProficient | Race.AcrobaticsProficient | Background.AcrobaticsProficient | Class.AcrobaticsProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedDexterity() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedDexterity() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedAnimalHandling()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (AnimalHandlingProficient | Race.AnimalHandlingProficient | Background.AnimalHandlingProficient | Class.AnimalHandlingProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedWisdom() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedWisdom() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedArcana()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (ArcanaProficient | Race.ArcanaProficient | Background.ArcanaProficient | Class.ArcanaProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedIntelligence() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedIntelligence() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedAthletics()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (AthleticsProficient | Race.AthleticsProficient | Background.AthleticsProficient | Class.AthleticsProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedStrength() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedStrength() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedDeception()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (DeceptionProficient | Race.DeceptionProficient | Background.DeceptionProficient | Class.DeceptionProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedCharisma() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedCharisma() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedHistory()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (HistoryProficient | Race.HistoryProficient | Background.HistoryProficient | Class.HistoryProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedIntelligence() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedIntelligence() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedInsight()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (InsightProficient | Race.InsightProficient | Background.InsightProficient | Class.InsightProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedWisdom() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedWisdom() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedIntimidation()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (IntimidationProficient | Race.IntimidationProficient | Background.IntimidationProficient | Class.IntimidationProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedCharisma() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedCharisma() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedInvestigation()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (InvestigationProficient | Race.InvestigationProficient | Background.InvestigationProficient | Class.InvestigationProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedIntelligence() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedIntelligence() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedMedicine()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (MedicineProficient | Race.MedicineProficient | Background.MedicineProficient | Class.MedicineProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedWisdom() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedWisdom() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedNature()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (NatureProficient | Race.NatureProficient | Background.NatureProficient | Class.NatureProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedIntelligence() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedIntelligence() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedPerception()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (PerceptionProficient | Race.PerceptionProficient | Background.PerceptionProficient | Class.PerceptionProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedWisdom() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedWisdom() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedPerformance()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (PerformanceProficient | Race.PerformanceProficient | Background.PerformanceProficient | Class.PerformanceProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedCharisma() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedCharisma() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedPersuasion()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (PersuasionProficient | Race.PersuasionProficient | Background.PersuasionProficient | Class.PersuasionProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedCharisma() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedCharisma() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedReligion()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (ReligionProficient | Race.ReligionProficient | Background.ReligionProficient | Class.ReligionProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedIntelligence() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedIntelligence() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedSleightOfHand()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (SleightOfHandProficient | Race.SleightOfHandProficient | Background.SleightOfHandProficient | Class.SleightOfHandProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedDexterity() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedDexterity() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedStealth()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (StealthProficient | Race.StealthProficient | Background.StealthProficient | Class.StealthProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedDexterity() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedDexterity() - 10) / 2);
            }

            return returnValue;
        }
        public int GetCombinedSurvival()
        {
            int returnValue = 0;

            // check if the character is proficient
            if (SurvivalProficient | Race.SurvivalProficient | Background.SurvivalProficient | Class.SurvivalProficient)
            {
                returnValue += 2;
            }

            // check if we have a bonus/malus from the characters stats
            if (GetCombinedWisdom() < 10)
            {
                returnValue -= 1;
            }
            else
            {
                returnValue += ((GetCombinedWisdom() - 10) / 2);
            }

            return returnValue;
        }

        public int GetCombinedHitPoints()
        {
            return Class.HitPoints + GetCombinedConstitution() / 2;
        }

        public int GetCombinedStrengthSavingThrow()
        {
            int returnValue = 0;
            if (Class.StrengthSavingThrow)
            {
                returnValue += 2;
            }

            return returnValue + GetCombinedStrength() / 2;
        }
        public int GetCombinedDexteritySavingThrow()
        {
            int returnValue = 0;
            if (Class.DexteritySavingThrow)
            {
                returnValue += 2;
            }

            return returnValue + GetCombinedDexterity() / 2;
        }
        public int GetCombinedConstitutionSavingThrow()
        {
            int returnValue = 0;
            if (Class.ConstitutionSavingThrow)
            {
                returnValue += 2;
            }

            return returnValue + GetCombinedConstitution() / 2;
        }
        public int GetCombinedIntelligenceSavingThrow()
        {
            int returnValue = 0;
            if (Class.IntelligenceSavingThrow)
            {
                returnValue += 2;
            }

            return returnValue + GetCombinedIntelligence() / 2;
        }
        public int GetCombinedWisdomSavingThrow()
        {
            int returnValue = 0;
            if (Class.WisdomSavingThrow)
            {
                returnValue += 2;
            }

            return returnValue + GetCombinedWisdom() / 2;
        }
        public int GetCombinedCharismaSavingThrow()
        {
            int returnValue = 0;
            if (Class.CharismaSavingThrow)
            {
                returnValue += 2;
            }

            return returnValue + GetCombinedCharisma() / 2;
        }
    }
}
