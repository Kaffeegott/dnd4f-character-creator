﻿namespace DnD4F_Character_Creator.Classes.Classes
{
    class Rogue : Class
    {
        // a Rogue can choose four proficiencies in skills from the list below
        // Acrobatics, Athletics, Deception, Insight, Intimidation, Investigation, Perception, Performance, Persuasion, Sleight of Hand, Stealth
        public readonly int MaxProficienciesCount = 4;

        public Rogue()
        {
            Name = "Rogue";

            Description = "";

            HitPoints = 8;

            DexteritySavingThrow = true;
            IntelligenceSavingThrow = true;
        }
    }
}
