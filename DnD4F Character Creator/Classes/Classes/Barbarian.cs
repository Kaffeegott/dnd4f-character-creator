﻿namespace DnD4F_Character_Creator.Classes.Classes
{
    class Barbarian : Class
    {
        // a barbarian can choose two proficiencies in skills from the list below
        // Animal Handling, Athletics, Intimidation, Nature, Perception, Survival
        public readonly int MaxProficienciesCount = 2;

        public Barbarian()
        {
            Name = "Barbarian";

            Description = "";

            HitPoints = 12;

            StrengthSavingThrow = true;
            ConstitutionSavingThrow = true;
        }
    }
}
