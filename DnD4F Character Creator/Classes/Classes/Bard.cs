﻿namespace DnD4F_Character_Creator.Classes.Classes
{
    class Bard : Class
    {
        // a bard can choose three proficiencies from every skill
        public readonly int MaxProficienciesCount = 3;

        public Bard()
        {
            Name = "Bard";

            Description = "";

            HitPoints = 8;

            DexteritySavingThrow = true;
            CharismaSavingThrow = true;
        }
    }
}
