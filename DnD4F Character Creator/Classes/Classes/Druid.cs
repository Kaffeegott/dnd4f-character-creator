﻿namespace DnD4F_Character_Creator.Classes.Classes
{
    class Druid : Class
    {
        // a Druid can choose two proficiencies in skills from the list below
        // Arcana, Animal Handling, Insight, Medicine, nature, Perception, Religion, Survival
        public readonly int MaxProficienciesCount = 2;

        public Druid()
        {
            Name = "Druid";

            Description = "";

            HitPoints = 8;

            IntelligenceSavingThrow = true;
            WisdomSavingThrow = true;
        }
    }
}
