﻿namespace DnD4F_Character_Creator.Classes.Classes
{
    class Cleric : Class
    {
        // a Cleric can choose two proficiencies in skills from the list below
        // History, Insight, Medicine, Persuasion, Religion
        public readonly int MaxProficienciesCount = 2;

        public Cleric()
        {
            Name = "Cleric";

            Description = "";

            HitPoints = 8;

            WisdomSavingThrow = true;
            CharismaSavingThrow = true;
        }
    }
}
