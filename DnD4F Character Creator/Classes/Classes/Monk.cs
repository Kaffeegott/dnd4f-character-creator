﻿namespace DnD4F_Character_Creator.Classes.Classes
{
    class Monk : Class
    {
        // a Monk can choose two proficiencies in skills from the list below
        // Acrobatics, Athletics, History, Insight, Religion, Stealth
        public readonly int MaxProficienciesCount = 2;

        public Monk()
        {
            Name = "Monk";

            Description = "";

            HitPoints = 8;

            StrengthSavingThrow = true;
            DexteritySavingThrow = true;
        }
    }
}
