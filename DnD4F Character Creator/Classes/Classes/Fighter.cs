﻿namespace DnD4F_Character_Creator.Classes.Classes
{
    class Fighter : Class
    {
        // a Fighter can choose two proficiencies in skills from the list below
        // Acrobatics, Animal Handling, Athletics, History, Insight, Intimidation, Perception, Survival
        public readonly int MaxProficienciesCount = 2;

        public Fighter()
        {
            Name = "Fighter";

            Description = "";

            HitPoints = 10;

            StrengthSavingThrow = true;
            ConstitutionSavingThrow = true;
        }
    }
}
