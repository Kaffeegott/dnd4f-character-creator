﻿namespace DnD4F_Character_Creator.Classes.Classes
{
    class Paladin : Class
    {
        // a Paladin can choose two proficiencies in skills from the list below
        // Athletics, Insight, Intimidation, Medicine, Persuasion, Religion
        public readonly int MaxProficienciesCount = 2;

        public Paladin()
        {
            Name = "Paladin";

            Description = "";

            HitPoints = 10;

            WisdomSavingThrow = true;
            CharismaSavingThrow = true;
        }
    }
}
