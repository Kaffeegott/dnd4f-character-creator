﻿namespace DnD4F_Character_Creator.Classes.Classes
{
    class Warlock : Class
    {
        // a Warlock can choose two proficiencies in skills from the list below
        // Arcana, Deception, History, Intimidation, Investigation, Nature, Religion
        public readonly int MaxProficienciesCount = 2;

        public Warlock()
        {
            Name = "Warlock";

            Description = "";

            HitPoints = 8;

            WisdomSavingThrow = true;
            CharismaSavingThrow = true;
        }
    }
}
