﻿namespace DnD4F_Character_Creator.Classes.Classes
{
    class Ranger : Class
    {
        // a Ranger can choose three proficiencies in skills from the list below
        // Animal Handling, Athletics, Insight, Investigation, Nature, Perception, Stealth, Survival
        public readonly int MaxProficienciesCount = 3;

        public Ranger()
        {
            Name = "Ranger";

            Description = "";

            HitPoints = 10;

            StrengthSavingThrow = true;
            DexteritySavingThrow = true;
        }
    }
}
