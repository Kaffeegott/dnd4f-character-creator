﻿namespace DnD4F_Character_Creator.Classes.Classes
{
    class Wizard : Class
    {
        // a Wizard can choose two proficiencies in skills from the list below
        // Arcana, History, Insight, Investigation, Medicine, Religion
        public readonly int MaxProficienciesCount = 2;

        public Wizard()
        {
            Name = "Wizard";

            Description = "";

            HitPoints = 6;

            IntelligenceSavingThrow = true;
            WisdomSavingThrow = true;
        }
    }
}
