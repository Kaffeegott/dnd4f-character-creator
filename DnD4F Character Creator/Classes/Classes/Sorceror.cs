﻿namespace DnD4F_Character_Creator.Classes.Classes
{
    class Sorceror : Class
    {
        // a Sorceror can choose two proficiencies in skills from the list below
        // Arcana, Deception, Insight, Intimidation, Persusaion, Religion
        public readonly int MaxProficienciesCount = 2;

        public Sorceror()
        {
            Name = "Sorceror";

            Description = "";

            HitPoints = 6;

            ConstitutionSavingThrow = true;
            CharismaSavingThrow = true;
        }
    }
}
