﻿namespace DnD4F_Character_Creator.Classes
{
    class Class
    {
        public string Name
        {
            get; set;
        }

        public string Description
        {
            get; set;
        }

        public int HitPoints
        {
            get; set;
        }

        public bool AcrobaticsProficient
        {
            get; set;
        }
        public bool AnimalHandlingProficient
        {
            get; set;
        }
        public bool ArcanaProficient
        {
            get; set;
        }
        public bool AthleticsProficient
        {
            get; set;
        }
        public bool DeceptionProficient
        {
            get; set;
        }
        public bool HistoryProficient
        {
            get; set;
        }
        public bool InsightProficient
        {
            get; set;
        }
        public bool IntimidationProficient
        {
            get; set;
        }
        public bool InvestigationProficient
        {
            get; set;
        }
        public bool MedicineProficient
        {
            get; set;
        }
        public bool NatureProficient
        {
            get; set;
        }
        public bool PerceptionProficient
        {
            get; set;
        }
        public bool PerformanceProficient
        {
            get; set;
        }
        public bool PersuasionProficient
        {
            get; set;
        }
        public bool ReligionProficient
        {
            get; set;
        }
        public bool SleightOfHandProficient
        {
            get; set;
        }
        public bool StealthProficient
        {
            get; set;
        }
        public bool SurvivalProficient
        {
            get; set;
        }

        public bool StrengthSavingThrow
        {
            get; set;
        }
        public bool DexteritySavingThrow
        {
            get; set;
        }
        public bool ConstitutionSavingThrow
        {
            get; set;
        }
        public bool IntelligenceSavingThrow
        {
            get; set;
        }
        public bool WisdomSavingThrow
        {
            get; set;
        }
        public bool CharismaSavingThrow
        {
            get; set;
        }
    }
}
