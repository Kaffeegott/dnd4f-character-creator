﻿namespace DnD4F_Character_Creator.Classes.Races
{
    class HalfElf : Race
    {
        public HalfElf()
        {
            Name = "Half-Elf";

            Description = "Half-elves are charismatic, and bear a resemblance to both their elvish and human parents and share many of the traits of each.";

            CharismaBonus = 2;
            // two user picked +1 for any ability

            Languages = new string[] { "Elvish" };
        }
    }
}
