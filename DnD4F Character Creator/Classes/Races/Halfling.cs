﻿namespace DnD4F_Character_Creator.Classes.Races
{
    class Halfling : Race
    {
        public Halfling()
        {
            Name = "Halfling";

            Description = "Halflings are small and nimble, half the height of a human, but fairly stout. They are cheerful and practical.";

            DexterityBonus = 2;

            SpeedBonus = -5;

            Languages = new string[] { "Halfling" };
        }
    }
}
