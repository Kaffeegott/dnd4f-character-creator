﻿namespace DnD4F_Character_Creator.Classes.Races
{
    class Tiefling : Race
    {
        public Tiefling()
        {
            Name = "Tiefling";

            Description = "Tieflings bear the distinct marks of their infernal ancestry: horns, a tail, pointed teeth, and solid-colored eyes. They are smart and charismatic.";

            IntelligenceBonus = 1;
            WisdomBonus = 2;

            Languages = new string[] { "Infernal" };
        }
    }
}
