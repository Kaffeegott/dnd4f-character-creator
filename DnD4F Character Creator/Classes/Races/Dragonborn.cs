﻿namespace DnD4F_Character_Creator.Classes.Races
{
    class Dragonborn : Race
    {
        public Dragonborn()
        {
            Name = "Dragonborn";

            Description = "Kin to dragons, dragonborn resemble humanoid dragons, without wings or tail and standing erect. They tend to make excellent warriors.";

            StrengthBonus = 2;
            CharismaBonus = 1;

            Languages = new string[] { "Draconic" };
        }
    }
}
