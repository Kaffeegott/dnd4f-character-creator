﻿namespace DnD4F_Character_Creator.Classes.Races
{
    class Elf : Race
    {
        public Elf()
        {
            Name = "Elf";

            Description = "Elves are graceful, magical creatures, with a slight build.";

            DexterityBonus = 2;

            PerceptionProficient = true;

            Languages = new string[] { "Elvish" };
        }
    }
}
