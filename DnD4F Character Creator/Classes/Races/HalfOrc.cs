﻿namespace DnD4F_Character_Creator.Classes.Races
{
    class HalfOrc : Race
    {
        public HalfOrc()
        {
            Name = "Half-Orc";

            Description = "Half-orcs are strong and bear an unmistakable resemblance to their orcish parent. They tend to make excellent warriors, especially Barbarians.";

            StrengthBonus = 2;
            ConstitutionBonus = 1;

            IntimidationProficient = true;

            Languages = new string[] { "Orc" };
        }
    }
}
