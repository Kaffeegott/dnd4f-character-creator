﻿namespace DnD4F_Character_Creator.Classes.Races
{
    class Gnome : Race
    {
        public Gnome()
        {
            Name = "Gnome";

            Description = "Gnomes are small, intelligent humanoids who live life with the utmost of enthusiasm.";

            IntelligenceBonus = 2;

            SpeedBonus = -5;

            Languages = new string[] { "Gnomish" };
        }
    }
}
