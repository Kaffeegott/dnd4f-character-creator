﻿namespace DnD4F_Character_Creator.Classes.Races
{
    class Dwarf : Race
    {
        public Dwarf()
        {
            Name = "Dwarf";

            Description = "Dwarves are short and stout and tend to be skilled warriors and craftmen in stone and metal.";

            ConstitutionBonus = 2;

            SpeedBonus = -5;

            Languages = new string[] { "Dwarvish" };
        }
    }
}
