﻿namespace DnD4F_Character_Creator.Classes.Races
{
    class Human : Race
    {
        public Human()
        {
            Name = "Human";

            Description = "Humans are physically diverse and highly adaptable. They excel in nearly every profession.";

            StrengthBonus = 1;
            DexterityBonus = 1;
            ConstitutionBonus = 1;
            IntelligenceBonus = 1;
            WisdomBonus = 1;
            CharismaBonus = 1;
        }
    }
}
