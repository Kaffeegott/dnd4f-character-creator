﻿namespace DnD4F_Character_Creator.Classes.Backgrounds
{
    class Hermit : Background
    {
        public Hermit()
        {
            Name = "Hermit";

            Description = "You lived in seclusion—either in a sheltered community such as a monastery, or entirely alone—for a formative part of your life. In your time apart from the clamor of society, you found quiet, solitude, and perhaps some of the answers you were looking for.";

            MedicineProficient = true;
            ReligionProficient = true;
        }
    }
}
