﻿namespace DnD4F_Character_Creator.Classes.Backgrounds
{
    class Sage : Background
    {
        public Sage()
        {
            Name = "Sage";

            Description = "You spent years learning the lore of the multiverse. You scoured manuscripts, studied scrolls, and listened to the greatest experts on the subjects that interest you. Your efforts have made you a master in your fields of study.";

            ArcanaProficient = true;
            HistoryProficient = true;
        }
    }
}
