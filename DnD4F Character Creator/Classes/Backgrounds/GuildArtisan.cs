﻿namespace DnD4F_Character_Creator.Classes.Backgrounds
{
    class GuildArtisan : Background
    {
        public GuildArtisan()
        {
            Name = "Guild Artisan";

            Description = "You are a member of an artisan’s guild, skilled in a particular field and closely associated with other artisans. You are a well-established part of the mercantile world, freed by talent and wealth from the constraints of a feudal social order.You learned your skills as an apprentice to a master artisan, under the sponsorship of your guild, until you became a master in your own right.";

            InsightProficient = true;
            PersuasionProficient = true;
        }
    }
}
