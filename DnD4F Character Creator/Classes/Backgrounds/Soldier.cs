﻿namespace DnD4F_Character_Creator.Classes.Backgrounds
{
    class Soldier : Background
    {
        public Soldier()
        {
            Name = "Soldier";

            Description = "War has been your life for as long as you care to remember. You trained as a youth, studied the use of weapons and armor, learned basic survival techniques, including how to stay alive on the battlefield. You might have been part of a standing national army or a mercenary company, or perhaps a member of a local militia who rose to prominence during a recent war.";

            AthleticsProficient = true;
            IntimidationProficient = true;
        }
    }
}
