﻿namespace DnD4F_Character_Creator.Classes.Backgrounds
{
    class FolkHero : Background
    {
        public FolkHero()
        {
            Name = "Folk Hero";

            Description = "You come from a humble social rank, but you are destined for so much more. Already the people of your home village regard you as their champion, and your destiny calls you to stand against the tyrants and monsters that threaten the common folk everywhere.";

            AnimalHandlingProficient = true;
            SurvivalProficient = true;
        }
    }
}
