﻿namespace DnD4F_Character_Creator.Classes.Backgrounds
{
    class Entertainer : Background
    {
        public Entertainer()
        {
            Name = "Entertainer";

            Description = "You thrive in front of an audience. You know how to entrance them, entertain them, and even inspire them. Your poetics can stir the hearts of those who hear you, awakening grief or joy, laughter or anger. Your music raises their spirits or captures their sorrow. Your dance steps captivate, your humour cuts to the quick. Whatever techniques you use, your art is your life.";

            AcrobaticsProficient = true;
            PerformanceProficient = true;
        }
    }
}
