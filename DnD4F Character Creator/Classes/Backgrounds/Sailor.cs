﻿namespace DnD4F_Character_Creator.Classes.Backgrounds
{
    class Sailor : Background
    {
        public Sailor()
        {
            Name = "Sailor";

            Description = "You sailed on a seagoing vessel for years. In that time, you faced down mighty storms, monsters of the deep, and those who wanted to sink your craft to the bottomless depths. Your first love is the distant line of the horizon, but the time has come to try your hand at something new.";

            AthleticsProficient = true;
            PerceptionProficient = true;
        }
    }
}
