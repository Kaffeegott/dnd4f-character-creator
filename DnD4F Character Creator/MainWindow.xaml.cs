﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DnD4F_Character_Creator.Classes;
using DnD4F_Character_Creator.Classes.Races;
using DnD4F_Character_Creator.Classes.Backgrounds;
using DnD4F_Character_Creator.Classes.Classes;

namespace DnD4F_Character_Creator
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // create a debug character
            Character debugCharacter = new Character
            {
                CharacterName = "Major Succ",
                PlayerName = "Sven",
                Alignment = "Chaotic Neutral",
                Race = new Elf(),
                Background = new Criminal(),
                Class = new Barbarian(),
                Age = 234,
                Sex = "Female",
                Height = 182,
                Weight = 70,
                HairColor = "Black",
                EyeColor = "Green",
                SkinColor = "Gold",
                Strength = 8,
                Dexterity = 16,
                Constitution = 11,
                Intelligence = 15,
                Wisdom = 12,
                Charisma = 14
            };

            System.Diagnostics.Debugger.Break();
        }
    }
}
